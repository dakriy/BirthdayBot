#!/usr/bin/python3
import urllib.request
import datetime
import time
import json
import logging
import random
import smtplib
from email.mime.text import MIMEText


logging.basicConfig(filename='/home/yid/BirthdayBot/bdaybot.log', level=logging.DEBUG)
try:
    year, month, day = time.strftime("%y,%m,%d").split(',')
    end1 = time.strptime("07/06/" + year, "%d/%m/%y")
    end2 = time.strptime("24/09/" + year, "%d/%m/%y")
    if datetime.datetime(*end1[:6]) <= datetime.datetime.now() <= datetime.datetime(*end2[:6]):
        quit()
    if int(month) < 7:
        term = str(int(year) - 1) + year
    else:
        term = year + str(int(year) + 1)

    response = urllib.request.urlopen("https://aswwu.com/server/search/" + term + "/birthday=" +
                                  '{:02d}'.format(datetime.date.today().month) +
                                  "-" +
                                  '{:02d}'.format(datetime.date.today().day))
    jsonString = response.read().decode('utf-8')
    thing = json.loads(jsonString)
    f = open('/home/yid/BirthdayBot/sayings', 'r')
    sayings = f.read().splitlines()
    f.close()
    for person in thing['results']:
        s = smtplib.SMTP('localhost')
        person['email'] = person['username'] + "@wallawalla.edu"
        to = person['email']
        From = "BirthdayBot@persignum.com"
        msg = MIMEText("Hello, " + person['full_name'] + "!\n\nI'm here to send you Happy Birthday wishes!\n\n" +
                       random.choice(sayings) + "\n\n\nI'm only a bot, so don't hate me!")
        msg['Subject'] = "Happy Birthday " + person['full_name']
        msg["From"] = From
        msg["To"] = to
        s.send_message(msg)
        s.quit()

except Exception as e:
    logging.exception(e)


